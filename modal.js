const modal = document.querySelector("#modal");
const searchIcon = document.querySelector("#search-icon");
const cancelIcon = document.querySelector("#cancel-icon");

document.querySelector("#opener").addEventListener("click", openModal);

document.querySelector("#closer").addEventListener("click", closeModalHandler);

function clickOutsideToClose(e) {
  if (e.target.id === "modal") closeModalHandler(e);
}

function openModal() {
  modal.showModal();
  cancelIcon.classList.toggle("hidden");
  searchIcon.classList.toggle("hidden");
  modal.addEventListener("click", clickOutsideToClose);
}

function closeModalHandler(e) {
  cancelIcon.classList.toggle("hidden");
  searchIcon.classList.toggle("hidden");
  modal.close();
}
